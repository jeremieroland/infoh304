# Dépôt git du cours INFOH304

Ce dépôt git reprend tout le matériel du cours INFOH304, en particulier
- Cours
    - Slides
    - Code de base (partie programmation)
- TP
    - Enoncés
    - Slides d'introduction
    - Code de base
    - Corrigés (publiés après les séances)

Attention: les fichiers sur ce dépôt git peuvent être mis à jour au fur et à mesure de l'année, donc si vous récupérez les fichiers en début d'année (que ce soit par téléchargement direct ou via un clone, comme celui présent sur la machine virtuelle), veuillez vérifier les mises à jour pour être sûr d'avoir la dernière version des fichiers.

Vous pouvez utiliser ce dépôt de 3 manières différentes:
- Par téléchargement manuel des fichiers
- Via la création d'un clone (déjà présent sur la machine virtuelle), ce qui vous permettra de récupérer automatiquement les mises à jour.
- Via la création d'un fork, ce qui vous permettra non seulement de récupérer automatiquement les mises à jour, mais aussi de sauvegarder votre travail en ligne (pour les TPs).

L'utilisation d'un dépôt git sera expliquée dans le cadre du projet du cours, mais vous trouverez ci-dessous les instructions pour
1) Installer et configurer git
2) Créer un clone de ce dépôt git (déjà réalisé sur la machine virtuelle) et mettre à jour ce clone
3) Créer un fork de ce dépôt git et l'utiliser


## Installation et configuration de git

Pour commencer, il faut avoir installé et configuré git.
- Si ce n'est déjà fait, installez git sur votre machine (notez que git est déjà installé sur la machine virtuelle): [guide d'installation](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- Créez un compte sur [GitLab](https://gitlab.com/users/sign_up)
- Renseignez votre nom d'utilisateur et votre adresse email git via les commandes suivantes (remplacez $EMAIL_ADDRESS et $USERNAME par vos identifiants GitLab)
```
git config --global user.email $EMAIL_ADDRESS
git config --global user.name $USERNAME
```
- Pour plus de facilité, créez une clé SSH et ajoutez-la dans votre compte GitLab (voir [instructions ici](https://docs.gitlab.com/ee/ssh) ou guidance)

## Création et mise à jour d'un clone du dépôt git

Si vous n'utilisez pas la machine virtuelle, (qui reprend déjà un clone de ce dépôt) vous pouvez cloner le dépôt avec la commande
```
git clone git@gitlab.com:jeremieroland/infoh304.git
```
Pour mettre à jour un clone existant en cours d'année, placez vous à la racine du dossier contenant le clone et utilisez la commande
```
git pull
```
Si vous avez modifié localement certains fichiers avant d'utiliser cette commande et que ces fichiers ont également été mis à jour dans le dépôt original, la commande échouera car elle ne pourra pas fusionner automatiquement les changements dans la version distante et la version locale. Dans ce cas, la solution la plus simple est de supprimer les changements locaux (éventuellement après avoir sauvegardé votre version modifiée ailleurs) avec la commande
```
git reset --hard
```
Après cela, la commande ``git pull`` devrait correctement mettre à jour vos fichiers.

Pour plus d'efficacité, il est néanmoins recommandé de créer un fork de ce dépôt, ce qui vous permettra d'y sauvegarder votre travail (vous ne pouvez pas le faire sur le dépôt original auquel vous n'avez accès qu'en lecture).

## Création et utilisation d'un fork

### Création du fork sur GitLab

- Rendez-vous sur la page d'accueil de ce dépôt ([lien](https://gitlab.com/jeremieroland/infoh304))
- Si vous n'êtes pas encore connecté avec votre compte, faites-le en cliquant en haut à droite sur "Sign in / Register"
- Cliquez sur le bouton "Fork" en haut à droite
- Cliquez sur "Select a namespace" sous le titre "Project URL", et choisissez votre username GitLab
- Choisissez un "Visibility level" (vous pouvez choisir "Private si vous ne comptez pas partager votre travail")
- Cliquez sur "Fork project"

A partir de là, vous pouvez cloner le fork sur votre ordinateur, mais si vous avez déjà créé un clone du dépôt original, ou si vous utilisez celui qui existe déjà sur la machine virtuelle, vous pouvez transformer un clone existant en fork. Les instructions pour ces deux cas de figure se trouvent ci-dessous.

### Clonage du fork sur votre ordinateur
- Depuis un terminal, naviguez vers le dossier où vous voudriez cloner le projet
```
cd nom/du/dossier
```
- Clonez le fork localement (remplacer $USERNAME par votre identifiant GitLab)
```
git clone git@gitlab.com:$USERNAME/infoh304.git
```
- Ajoutez le dépôt original comme "remote upstream" pour pouvoir récupérer les mises à jour (voir "Utilisation du fork")
```
cd infoh304
git remote add upstream git@gitlab.com:jeremieroland/infoh304.git
```

### Transformation d'un clone existant en fork

Si vous utilisiez jusqu'à présent un clone du dépôt git original (que vous l'ayez créé vous-même ou que ce soit le clone présent sur la machine virtuelle), vous pouvez le transformer en fork.
- Depuis un terminal, naviguez vers le dossier où se trouve le clone
```
cd nom/du/dossier/infoh304
```
- Renommer l'actuel "remote origin" (dépôt original) en "upstream", ce qui vous permettra de récupérer les mises à jour (voir "Utilisation du fork")
```
git remote rename origin upstream
```
- Ajoutez votre fork comme "remote origin" (remplacer $USERNAME par votre identifiant GitLab), ce qui vous permettra de sauvegarder votre travail dans GitLab
```
git remote add origin git@gitlab.com:$USERNAME/infoh304.git
```
- Configurez votre fork comme remote par défaut
```
git push --set-upstream origin/main
```

### Utilisation du fork
- Au début de chaque TP, vous pouvez récupérer les mises à jour (énoncés et code de base du nouveau TP) depuis le dépôt original avec la commande
```
git pull --no-edit upstream main
```
- Vous pouvez alors travailler localement sur le TP en modifiant ou ajoutant des fichiers
- A la fin du TP (ou au fur et à mesure de votre travail), vous pouvez sauvegarder votre travail dans votre fork du dépôt INFOH304, par exemple via les commandes
```
git add . 
git commit -a -m "Travail sur le TP04"
git push
```


### Remarque
La commande `git pull --no-edit upstream main` incluant un merge depuis un autre dépôt, il se peut qu'elle crée des conflits. Cela ne devrait néanmoins pas être le cas car la plupart des mises à jour du dépôt original ne consisteront qu'en l'ajout de nouveaux fichiers, sans modification des fichiers existants.

Pour éviter tout souci, assurez-vous néanmoins que vos fichiers locaux soient bien à jour par rapport à votre fork avant de réaliser le pull depuis le dépôt original, en particulier
- Si vous avez modifié les fichiers localement, vous devez pousser les changements vers votre fork sur GitLab via les commandes `git add [...]`, `git commit [...]`, et `git push` comme indiqué ci-dessus (sinon votre remote sera en retard sur vos fichiers locaux)
- Si vous avez travaillé sur une autre machine à partir de laquelle vous avez modifié les fichiers de votre fork sur GitLab, récupérez d'abord les changements avec un `git pull` (sinon vos fichiers locaux seront en retard sur votre remote)

Notez qu'en travaillant de la sorte, les fichiers que vous avez modifiés lors de votre travail ne seront pas écrasés par la version dans le dépôt original lorsque vous effectuerez la commande `git pull --no-edit upstream main`, et même en cas d'erreur de manipulation, ce n'est pas grave puisque vous pourrez trouver les anciennes versions des fichiers sur votre fork.

