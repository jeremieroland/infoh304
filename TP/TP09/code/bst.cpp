#include "bst.h"

bool BinarySearchTree::isEmpty()
{
	return root==NULL;
}
	
void BinarySearchTree::print_tree()
{
	cout << "Représentation graphique de l'arbre :" << endl;
	vector<string> value_lines;
	vector<string> split_lines;
	vector<string> drop_lines;
	create_tree_lines(root, value_lines, split_lines, drop_lines);
	for (int i=0; i<value_lines.size(); i++)
	{
		if(i>0)
			cout << drop_lines[i] << endl;
		cout << value_lines[i] << endl;
		if(i<value_lines.size()-1)
			cout << split_lines[i] << endl;
	}
	cout << endl;
}

pair<int,int> BinarySearchTree::create_tree_lines(Node* start, vector<string>& value_lines, vector<string>& split_lines, vector<string>& drop_lines, int depth, int previous_width)
{
	if (start == NULL)
		return {0, -1};
		
	if (value_lines.size()<=depth)
	{
		value_lines.push_back("");
		split_lines.push_back("");
		drop_lines.push_back("");
	}
	
	if (value_lines[depth].size()<previous_width)
	{
		for(int i=value_lines[depth].size(); i<previous_width; i++)
		{
			value_lines[depth] += " ";
			split_lines[depth] += " ";
			drop_lines[depth] += " ";
		}
	}
	
	pair<int,int> retleft = create_tree_lines(start->getLeft(), value_lines, split_lines, drop_lines, depth+1, previous_width);
	int width_of_left_sub_tree = retleft.first;
	int center_left = retleft.second;
	
	int real_left_width = width_of_left_sub_tree + 1;
	if (width_of_left_sub_tree == 0)
		real_left_width = 0;
	int new_previous_width = previous_width + real_left_width;
		
	pair<int,int> retright = create_tree_lines(start->getRight(), value_lines, split_lines, drop_lines, depth+1, new_previous_width);
	int width_of_right_sub_tree = retright.first;
	int center_right = retright.second;
	
	int center = (center_right - center_left + real_left_width)/2 + center_left;
	if (center_left == -1)
		center = center_right;
	else if (center_right == -1)
		center = center_left;
	
	string value = to_string(start->getContent());
	int value_length = value.size();
	int total_width = width_of_left_sub_tree + width_of_right_sub_tree;
	if (width_of_left_sub_tree != 0 && width_of_right_sub_tree != 0)
		total_width += 1;
	if (total_width < value_length)
	{
		total_width = value_length;
		center = total_width/2;
	}
	
	int begin_of_value = center - ceil((value_length-1)/2.);
	
	for (int i=0; i<total_width; i++)
	{
		if (i >= begin_of_value && i < begin_of_value+value_length)
		{
			value_lines[depth] += value[i-begin_of_value];
		} else {
			value_lines[depth] += " ";
		}
			
		if (i == center)
			drop_lines[depth] += "|";
		else
			drop_lines[depth] += " ";
		
		if (i == center) {
			if(start->getLeft() == NULL && start->getRight() == NULL)
				split_lines[depth] += " ";
			else
				split_lines[depth] += "|";
		} else if (i > center_left && i<center) {
			if(start->getLeft() != NULL) {
				split_lines[depth] += "_";
			} else {
				split_lines[depth] += " ";
			}
		
		} else if (i > center && i < center_right + real_left_width) {
			if(start->getRight() != NULL) {
				split_lines[depth] += "_";
			} else {
				split_lines[depth] += " ";
			}
		} else {
			split_lines[depth] += " ";
		}
	}
	value_lines[depth] += " ";
	drop_lines[depth] += " ";
	split_lines[depth] += " ";
	
	return {total_width, center};
}

void BinarySearchTree::insert_element(int content)
{
	//New node
	Node* n=new Node(content);
	
	if(isEmpty())
		root = n;
	else
	{
		Node* pointer = root;
		
		while( pointer!=NULL )
		{
			n->setParent(pointer);
			if(n->getContent() > pointer->getContent())
				pointer = pointer->getRight();
			else
				pointer = pointer->getLeft();
		}
		
		if(n->getContent() > n->getParent()->getContent())
			n->getParent()->setRight(n);
		else
			n->getParent()->setLeft(n);
	}
}

void BinarySearchTree::inorder(Node* location)
{

}

void BinarySearchTree::print_inorder()
{
	cout << "Contenu de l'arbre :";
	inorder(root);
	cout << endl;
}

Node* BinarySearchTree::tree_search(int content)
{
	return tree_search(content, root);
}

Node* BinarySearchTree::tree_search(int content, Node* location)
{
	return NULL;
}

void BinarySearchTree::delete_no_child(Node* location)
{
	cout << "Suppression d'un noeud sans enfant" << endl;
}

void BinarySearchTree::delete_left_child(Node* location)
{
	cout << "Suppression d'un noeud ayant seulement un enfant de gauche" << endl;
}

void BinarySearchTree::delete_right_child(Node* location)
{
	cout << "Suppression d'un noeud ayant seulement un enfant de droite" << endl;
}

void BinarySearchTree::delete_two_children(Node* location)
{
	cout << "Suppression d'un noeud ayant deux enfants" << endl;
}

void BinarySearchTree::delete_element(int content)
{
	
}
