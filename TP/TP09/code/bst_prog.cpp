#include <random>
#include "bst.h"

int main()
{
	int entier;
	BinarySearchTree bst;
	
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<int> r(0, 1000);

	for(int i=1;i<=10;i++)
	{
		entier=r(gen);
		cout << "Insertion de l'entier " << entier << endl;
		bst.insert_element(entier);
		bst.print_tree();
	}
	
	return 0;
}
