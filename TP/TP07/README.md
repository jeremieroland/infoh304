# TP 07 - Tri par fusion

- Implémentation du tri par fusion (Mergesort) en C++
	- Vidéo d'introduction: [Lien](https://universitelibrebruxelles-my.sharepoint.com/:v:/r/personal/jeremie_roland_ulb_be/Documents/INFOH304/TPs/TP%2007a%20-%20Tri%20par%20fusion.mp4?csf=1&web=1&e=6UYcQl)
- Comparaison de performance avec le tri par insertion et le tri de la STL
