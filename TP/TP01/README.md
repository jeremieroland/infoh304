# TP 01 - Langage C (1) - Introduction

- Vidéo d'introduction : [Lien](https://universitelibrebruxelles-my.sharepoint.com/:v:/r/personal/jeremie_roland_ulb_be/Documents/INFOH304/TPs/TP%2001a%20-%20Langage%20C%20(1)%20-%20Introduction%20au%20C.mp4?csf=1&web=1&e=1f26T5&isSPOFile=1)
	- Tableaux (arrays)
	- Chaînes de caractères (string arrays)
- Chaînes de caractères
- Entrées/sorties en C: scanf et printf
- Manipulation de tableaux
- Paramètres de la ligne de commande
- Table ASCII
