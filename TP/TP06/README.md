# TP 06 - Complexité - Théorème Maître

- Fonctions et leur complexité
	- Notations grand-O, grand-Omega et grand-Theta
	- Vidéo d'introduction: [Lien](https://universitelibrebruxelles-my.sharepoint.com/:v:/r/personal/jeremie_roland_ulb_be/Documents/INFOH304/TPs/TP06a%20-%20Exercices%201-2%20-%20Notations%20asymptotiques.mkv?csf=1&web=1&e=SNjZoV)
- Application du théorème maître - Arbres de récurrence
	- Vidéo d'introduction: [Lien](https://universitelibrebruxelles-my.sharepoint.com/:v:/r/personal/jeremie_roland_ulb_be/Documents/INFOH304/TPs/TP06b%20-%20Exercices%203-6%20-%20Arbre%20de%20re%CC%81currence%20-%20The%CC%81ore%CC%80me%20mai%CC%82tre.mkv?csf=1&web=1&e=r8jTSd)
- Pour aller plus loin: Méthode par substitution
	- Vidéo d'introduction: [Lien](https://universitelibrebruxelles-my.sharepoint.com/:v:/r/personal/jeremie_roland_ulb_be/Documents/INFOH304/TPs/TP06c%20-%20Pour%20aller%20plus%20loin%20-%20Me%CC%81thode%20par%20substitution.mkv?csf=1&web=1&e=Kngbym)
