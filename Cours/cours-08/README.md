# Cours 08 - Quicksort - Borne inférieure pour le tri - tri linéaire

- Algorithmes de tri
	- Rappels
	- Tri rapide (Quicksort)
	- Borne inférieure pour le tri
	- Tri linéaire
