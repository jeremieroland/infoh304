# Cours 06 - Structures de données - Tas

- Structures de données - Introduction
	- Ensemble dynamique
	- Structure de données
	- Types de données abstrait
	- Exercice: coût des opérations
- Tas (heaps)
	- Notion de tas, queues de priorité
	- Tri par tas
- Structures de données - Techniques
	- Doublement de tableau
	- Choix et conception - Invariant de représentation
