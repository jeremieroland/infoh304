#include <iostream>
using std::cout;
using std::endl;

class Entier {
public:
	int val;
};

int main() {
	Entier* O1 = new Entier();
	O1->val=1;
	Entier* O2;
	O2=O1;
	cout << "O1 vaut " << O1->val << endl;
	cout << "O2 vaut " << O2->val << endl;
	O2->val=2;
	cout << "O1 vaut " << O1->val << endl;
	cout << "O2 vaut " << O2->val << endl;
	delete O1;
}
