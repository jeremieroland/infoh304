#include <iostream>
using namespace std;

int produit(int a, int b);
int produit(int a, int b, int c);

int main() {
	int x=2, y=3, z=4;
	cout << "Le produit de x et y vaut " << produit(x,y) << endl;
	cout << "Le produit de x, y et z vaut " << produit(x,y,z) << endl;
}

int produit(int a, int b)
	{ return a*b; }

int produit(int a, int b, int c)
	{ return a*b*c; }
