#include <iostream>
using std::cout;
using std::endl;

class Entier {
public:
	Entier() {
		cout << "Construction d'un Entier" << endl;
	}
	Entier( int valInit ) {
		cout << "Construction d'un Entier de valeur " << valInit << endl;
		val = valInit;
	}
	int val;
};

int main() {
	Entier* O1 = new Entier( 3 );
	Entier O2( 5 );
	Entier O3;
	/* Utilisation des entiers */
	delete O1;
}