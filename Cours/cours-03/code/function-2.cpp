#include <iostream>
using namespace std;

int produit(int a, int b, int c = 1);

int main() {
	int x=2, y=3, z=4;
	cout << "Le produit de x et y vaut " << produit(x,y) << endl;
	cout << "Le produit de x, y et z vaut " << produit(x,y,z) << endl;
}

int produit(int a, int b, int c)
	{ return a*b*c; }
