#include <iostream>
using namespace std;

/* echange: ne fait rien! */
void echange(int a, int b)
{
    int temp = a;
    a = b;
    b = temp;
}

int main()
{
    int x = 1, y = 2;
    echange(x, y);
    cout << "x = " << x;
    cout << ", y = " << y << endl;
    return 0;
}