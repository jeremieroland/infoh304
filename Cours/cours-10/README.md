# Cours 10 - Tables de hachage

- Tables de hachage
	- Introduction : Dictionnaires
	- Tables à adressage direct
	- Chaînage
	- Adressage ouvert
- Pour aller plus loin
	- Hachage cryptographique: [vidéo](https://universitelibrebruxelles-my.sharepoint.com/:v:/r/personal/jeremie_roland_ulb_be/Documents/INFOH304/Cours%20th%C3%A9oriques/Cours%2010a%20-%20Hachage%20cryptographique.mp4?csf=1&web=1&e=z9vOxF)
