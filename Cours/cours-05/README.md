# Cours 05 - Algorithmique - Notion de complexité

- Algorithmique
	- Introduction
	- Notion d'algorithme et de complexité
	- Motivation: recherche de pic
	- Algorithmes de tri: tri par insertion
- Pour aller plus loin
	- Classes de complexité: [vidéo](https://universitelibrebruxelles-my.sharepoint.com/:v:/r/personal/jeremie_roland_ulb_be/Documents/INFOH304/Cours%20th%C3%A9oriques/Cours%2005a%20-%20Classes%20de%20complexite%CC%81.mp4?csf=1&web=1&e=2s2gCe)
