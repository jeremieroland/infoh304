#include <stdio.h>

/* echange: tentative ratée d'échange des deux arguments */
void echange(int a, int b)
{
	int temp = a;
	a = b;
	b = temp;
}

int main()
{
	int premier = 1, deuxieme = 2;
	echange(premier, deuxieme);
	printf("premier vaut %i, deuxieme vaut %i.\n", premier, deuxieme);
	return 0;
}