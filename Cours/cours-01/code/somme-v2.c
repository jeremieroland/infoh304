# include <stdio.h>

int somme(int a, int b)
{
	static int compteur = 0;
	int somme = a+b;
	++compteur;
	printf("Le compteur vaut %i\n",compteur);
	return somme;
}

int main()
{
	printf("La somme vaut %i\n",somme(1,1));
	printf("Le compteur vaut %i\n",compteur);
	printf("La somme vaut %i\n",somme(2,2));
	return 0;
}
