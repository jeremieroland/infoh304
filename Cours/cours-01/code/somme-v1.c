#include <stdio.h>
int somme(int a, int b);

int main()
{
	int terme1 = 5, terme2 = 7;
	int resultat = somme(terme1, terme2);
	printf("La somme des termes vaut %i.\n", resultat);
	return 0;
}

int somme(int a, int b)
{
	return a+b;
}
