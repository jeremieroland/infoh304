#include <stdio.h>
int main()
{
	char premiere = 'a';
	int nbre_lettres = 26;
	const float avogadro = 6.0221415e23;
	const double pi = 3.14159265358979323846;
	printf("Premiere lettre de l'alphabet: %c\n", premiere);
	printf("Nombre de lettres: %10i\n", nbre_lettres);
	printf("Le nombre d'Avogadro vaut %11.3e\n", avogadro);
	printf("La constante pi vaut %10.6f\n", pi);
	return 0;
}