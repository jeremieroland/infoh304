# Cours 11 - Programmation dynamique (1)

- Programmation dynamique (1)
	- Prélude
	- Problème de pavage
	- Principes généraux
	- Justification de texte
	- Blackjack: [vidéo](https://universitelibrebruxelles-my.sharepoint.com/:v:/r/personal/jeremie_roland_ulb_be/Documents/INFOH304/Cours%20th%C3%A9oriques/Cours%2011a%20-%20Blackjack%20par%20programmation%20dynamique.mp4?csf=1&web=1&e=RZHFLg)
