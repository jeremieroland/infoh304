#include <iostream>
#include <string>
#include "MembreULB.h"
using std::cout;
using std::endl;

int main() {
	MembreULB* pmembre = new EtudiantULB( "Dupont", "Laurent", "IRCI3-B" );
	// Erreur: un MembreULB n'a pas de cursus!
	cout << pmembre->getCursus() << endl;
	// Static cast:
	EtudiantULB* petudiant = (EtudiantULB*) pmembre;
	cout << petudiant->getCursus() << endl;
	// Dynamic cast:
	EtudiantULB* petudiant2 = dynamic_cast<EtudiantULB*>( pmembre );
	if ( petudiant2 != NULL )
		cout << petudiant2->getCursus() << endl;
	delete pmembre;
	cout << "Fin du programme" << endl;
	return 0;
}
