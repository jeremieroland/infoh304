#include <iostream>
#include <string>
#include "MembreULB.h"
using std::cout;
using std::endl;

int main() {
	EtudiantULB etudiant( "Dupont", "Laurent", "IRCI3-B" );
	MembreULB & membre = etudiant;
	etudiant.print(); cout << endl;
	membre.print(); cout << endl;
	cout << "Fin du programme" << endl;
	return 0;
}