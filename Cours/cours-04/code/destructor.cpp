#include <iostream>
#include <string>
#include "MembreULB.h"
using std::cout;
using std::endl;

int main() {
	EtudiantULB* petudiant = new EtudiantULB( "Dupont", "Laurent", "IRCI3-B" );
	MembreULB* pmembre = petudiant;
	delete pmembre;
	cout << "Fin du programme" << endl;
	return 0;
}