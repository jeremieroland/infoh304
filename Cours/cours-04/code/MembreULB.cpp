#include "MembreULB.h"

MembreULB::MembreULB(string nomInit, string prenomInit) : nom(nomInit), prenom(prenomInit) {
	cout << "Construction d'un MembreULB" << endl;
	matricule = 1401234;
}

MembreULB::~MembreULB() {
	cout << "Destruction d'un MembreULB" << endl;
}

string MembreULB::getNom() const {
	return nom;
}

void MembreULB::setNom(string nouveauNom) {
	nom=nouveauNom;
}

string MembreULB::getPrenom() const {
	return prenom;
}

void MembreULB::setPrenom(string nouveauPrenom) {
	prenom=nouveauPrenom;
}

void MembreULB::print( ostream & out ) const {
	out << nom << ", " << prenom << ", " << matricule;
}

EtudiantULB::EtudiantULB(string nomInit, string prenomInit, string cursusInit) : MembreULB(nomInit, prenomInit), cursus(cursusInit) {
		cout << "Construction d'un EtudiantULB" << endl;
}

EtudiantULB::~EtudiantULB() {
	cout << "Destruction d'un EtudiantULB" << endl;
}

string EtudiantULB::getCursus() const {
	return cursus;
}

void EtudiantULB::setCursus(string nouveauCursus) {
	cursus=nouveauCursus;
}

void EtudiantULB::print( ostream & out ) const {
	MembreULB::print( out );
	out << ", " << cursus;
}

EmployeULB::EmployeULB(string nomInit, string prenomInit, string fonctionInit) : MembreULB(nomInit, prenomInit), fonction(fonctionInit) {
	cout << "Construction d'un EmployeULB" << endl;
	}

EmployeULB::~EmployeULB() {
	cout << "Destruction d'un EmployeULB" << endl;
}

string EmployeULB::getFonction() const {
	return fonction;
}

void EmployeULB::setFonction(string nouvelleFonction) {
	fonction=nouvelleFonction;
}

void EmployeULB::print( ostream & out ) const {
	MembreULB::print( out );
	out << ", " << fonction;
}

// Appel au constructeur ancêtre pour héritage multiple (nécessite héritage virtuel)
// EmployeEtudiantULB::EmployeEtudiantULB(string nomInit, string prenomInit, string cursusInit, string fonctionInit): MembreULB(prenomInit, nomInit), EtudiantULB("NomEtudiant", "PrenomEtudiant", cursusInit), EmployeULB("NomEmploye", "PrenomEmploye", fonctionInit) {
EmployeEtudiantULB::EmployeEtudiantULB(string nomInit, string prenomInit, string cursusInit, string fonctionInit): EtudiantULB(prenomInit, nomInit, cursusInit), EmployeULB("NomEmploye", "PrenomEmploye", fonctionInit) {
	cout << "Construction d'un EmployeEtudiantULB" << endl;
}

EmployeEtudiantULB::~EmployeEtudiantULB() {
	cout << "Destruction d'un EmployeEtudiantULB" << endl;
}

void EmployeEtudiantULB::print( ostream & out ) const {
	EtudiantULB::print( out );
	out << ", " << getFonction();
}
