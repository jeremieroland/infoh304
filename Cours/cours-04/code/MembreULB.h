#include <iostream>
#include <string>
using std::cout;
using std::endl;
using std::ostream;
using std::string;

class MembreULB {
	string prenom;
	string nom;
public:
	int matricule;
	MembreULB(string nomInit="", string prenomInit="");
	~MembreULB();
	string getPrenom() const;
	void setPrenom(string nouveauPrenom);
	string getNom() const;
	void setNom(string nouveauNom);
	void print(ostream & out = cout ) const;
};

class EtudiantULB : public MembreULB {
	string cursus;
public:
	EtudiantULB(string nomInit="", string prenomInit="", string cursusInit="");
	~EtudiantULB();
	string getCursus() const;
	void setCursus(string nouveauCursus);
	void print(ostream & out = cout ) const;
};

class EmployeULB : public MembreULB {
	string fonction;
public:
	EmployeULB(string nomInit="", string prenomInit="", string fonctionInit="");
	~EmployeULB();
	string getFonction() const;
	void setFonction(string nouvelleFonction);
	void print(ostream & out = cout ) const;
};

class EmployeEtudiantULB : public EtudiantULB, public EmployeULB {
public:
	EmployeEtudiantULB(string nomInit="", string prenomInit="", string cursusInit="", string fonctionInit="");
	~EmployeEtudiantULB();
	void print(ostream & out = cout ) const;
};
