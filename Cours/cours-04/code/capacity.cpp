#include <vector>
#include <iostream>
using std::vector;
using std::cout;
using std::endl;

template <typename Container>
void print( const Container & c )
{
	typename Container::const_iterator itr;
	for ( itr = c.begin(); itr != c.end(); ++itr )
		cout << *itr << " ";
	cout << endl;
}

int main () {
	vector<int> tableau( 5 );
	print(tableau);
	cout << "Taille: " << tableau.size() << ", capacite: " << tableau.capacity() << endl;
	
	tableau.push_back( 32 );
	print(tableau);
	cout << "Taille: " << tableau.size() << ", capacite: " << tableau.capacity() << endl;
	return 0;  
}