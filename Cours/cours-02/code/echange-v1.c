#include <stdio.h>

/* echange: ne fait rien! */
void echange(int a, int b)
{
    int temp = a;
    a = b;
    b = temp;
}

int main()
{
    int x = 1, y = 2;
    echange(x, y);
    printf("x = %i, y = %i.\n", x, y);
    return 0;
}