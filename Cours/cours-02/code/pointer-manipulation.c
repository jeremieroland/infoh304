int main()
{
    int i, x = 10, y = 20, z[15];
    int* ip;

    for ( i=0; i<15; i++ )
        z[i] = i+1;

    ip = &x;
    y = *ip;
    *ip = 0;
    ip = &z[0];

    *ip = *ip+1;
    ip = ip+1;

    return 0;
}
