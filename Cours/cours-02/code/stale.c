#include <stdio.h>
#include <stdlib.h>


int main()
{
	int* a = malloc(sizeof(int));
	int* b = a;
	*a = 3;
	printf("*a = %d\n", *a);
	
	free(b);
	printf("*a = %d\n", *a);
	
	int* c = malloc(sizeof(int));
	*c = 4;
	printf("*a = %d\n", *a);

	return 0;
}
