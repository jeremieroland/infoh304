#include <stdio.h>

int* func()
{
	int a = 123;
	return &a;
}

void overwrite()
{
	int x = 456;
}

int main()
{
	int* pa = func();
	printf("*pa = %d\n", *pa);
	overwrite();
	printf("*pa = %d\n", *pa);
	return 0;
}
