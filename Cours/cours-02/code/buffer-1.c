#include <stdio.h>
#include <string.h>
#define LONGMAX 5

int main()
{
	char chaine1[] = "Hello, World!";
	char chaine2[LONGMAX];
	strcpy(chaine2, chaine1);
	printf("%s\n",chaine2);
	return 0;
}