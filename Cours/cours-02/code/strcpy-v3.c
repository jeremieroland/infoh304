/* strcpy: copie t vers s (v3) */
void strcpy(char* s, char* t)
{
	while ( (*s++ = *t++) != '\0' )
		;
}