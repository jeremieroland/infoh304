/* strlen: calcule la longueur de la chaine s (v1) */
int strlen(char* s)
{
	int n;
	for ( n=0; *s; n++, s++ )
		;
	return n;
}
