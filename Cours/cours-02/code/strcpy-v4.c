/* strcpy: copie t vers s (v4) */
void strcpy(char* s, char* t)
{
	while ( *s++ = *t++ )
		;
}