# Cours 12 - Programmation dynamique (2)

- Programmation dynamique (2)
	- Définitions des sous-problèmes
	- Algorithmes gloutons
	- Exemples avancés
		- Doigté de piano
		- Flappy Bird: [vidéo](https://universitelibrebruxelles-my.sharepoint.com/:v:/r/personal/jeremie_roland_ulb_be/Documents/INFOH304/Cours%20th%C3%A9oriques/Cours%2012a%20-%20Flappy%20Bird%20par%20programmation%20dynamique.mp4?csf=1&web=1&e=4BkdKf)
